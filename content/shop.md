+++
title = "Shop"
+++

## DIY Bike Shop

What we are
: We provide the tools, space & guidance. You provide the project.
The Sopo Bike Shop is a Safe Space for you to learn how to fix your own bicycle
guided by our volunteers.
Sopo has a huge wall of used parts, that may be ready to install or ready to
part out and reuse, all the pro tools you will need and bike work stations for
you to work on your project.
We also have ready-to-ride and needs-some-work bicycles for sale.
Inventory changes daily and all bikes are priced by the community.
Volunteers and shop managers are available to guide you in your project.
They will not do the work for you but they will teach you how to do it.

What we aren't
: We are not a traditional bike shop or for profit store. You
can't drop off your bike for service.

Location
: {{< addr >}}

Hours
: {{< hours >}}

Pricing
: Suggested donation is $5 per shop hour and per part used.
Frames and bikes are priced at the shop.

Shop Rules
: The shop is a Safe Space.
We do not tolerate violence, harassment or hate speech.
So come hang & chill with us.
All of us.
