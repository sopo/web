+++
title = "Programs"
+++

## How we help

Sopo Bicycle Cooperative is a non-profit, community-based, DIY bicycle repair
shop in Atlanta, GA that makes transportation affordable, accessible, and
sustainable.
Contact us at [info@sopobikes.org] to support or
propose a program.

### Outreach

As a community-based organization, all our programs must support, benefit and
improve the community.

### Persons in need

We provide free bikes to persons in need in order to improve their
transportation options.

### Bike Parking Racks

Contact us at [info@sopobikes.org] to start your bike rack project.
We do design, planning & installation.


[info@sopobikes.org]: mailto:info@sopobikes.org
