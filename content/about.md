+++
title = "About"
+++

## About Sopo

### What is Sopo Bicycle Cooperative?

Sopo Bicycle Cooperative is a nonprofit tax-exempt 501(c)3 charitable
organization that facilitates the collective ownership and operation of a {{<
abbr title="Do It Yourself">}}DIY{{< /abbr >}}
bicycle repair shop. No prior experience is needed; we will teach you! All
members "turn wrenches" and participate in supporting the co-op.

### What is Sopo's mission?

Our mission is to create equitable access to cycling by providing no/low-cost
bicycle maintenance services and education.

### Where is the shop?

The shop is located at The Met {{< addr >}} just across
the street from the West End MARTA station.

### What are the shop hours?

{{< hours >}}

### Can I donate my bicycle or bike parts?

Yes, we accept donations of adult bicycles, parts, accessories, bike tools &
funds.
We do not accept kids bikes but our friends do, please contact [Free Bikes
4 Kids](https://fb4k.org/).

### Do you recycle bicycles that are no longer rebuildable?

We accept bicycles that are not road worthy but still have usable parts.
If your bicycle is too damaged or rusted to repair, please take it to
[CHARM](https://livethrive.org/charm/) for recycling.

### Can I donate clothes?

Yes, but please wash them first.

### What is the suggested donation?

We suggest a $5 donation per hour for use of tools, grease, and other items and
$5 per small part.
However, no one is turned away for lack of cash or resources.
Frames and bicycles are priced at the shop and usually start at $20.

### Do you sell complete bikes?

Yes, used bikes that are ready to ride are priced at the shop.

### What can I do at Sopo?

You can access tools and knowledge to work on your bike in a laid-back
environment.

### Will you fix my bike for me?

No. Someone at Sopo will guide you while you fix your own bike.
