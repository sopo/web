+++
title = "DIY Bike Shop"
+++

{{< hero caption=""
         height="45vh"
         img="/images/hero.jpg"
         donate=true >}}

## Atlanta's Bike Co-op

Sopo Bicycle Cooperative is a non-profit, community-based, DIY bicycle
repair shop in Atlanta, GA that makes transportation affordable, accessible, and
sustainable

Location
: {{< addr >}}

Hours
: {{< hours >}}

{{< /hero >}}

{{< flex justify-content="space-between" >}}

{{< wrap icon="bicycle" title="About" >}}
We provide the tools, space & guidance. You provide the project.
Sopo is a safe space with simple rules: treat all with respect, be kind, and be
chill.

[Read More](/about)
{{< /wrap >}}

{{< wrap icon="cogs" title="Programs" >}}
We work with individuals, non-profits, government and corporations to install
parking racks, donate bikes, host clinics, and support refugee, employment &
community charities.

[Learn More](/programs)
{{< /wrap >}}

{{< wrap icon="users" title="Volunteer" >}}
Sopo runs on volunteer power, and we always need more.
We have opportunities to run a shop, build bikes, or teach bicycle maintenance.

[Help Out](/volunteer)
{{< /wrap >}}

{{< /flex >}}
