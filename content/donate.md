+++
title = "Donate"
+++

## Ways to Donate


{{< flex justify-content="space-between" >}}

{{< wrap icon="money" title="Funds" >}}
Stop by Sopo & donate any amount of cash or credit card payment.
You can also donate online using one of the links below.
{{< /wrap >}}

{{< wrap icon="clock-o" title="Time" >}}
Donate your time by volunteering at our shop. No experience needed, just a
willingness to help your community.

[Volunteer](/volunteer)
{{< /wrap >}}

{{< wrap icon="bicycle" title="Bikes & Parts" >}}
We accept all bikes and parts that are still useable.
Drop them off at shop during open hours.

{{< hours >}}
{{< /wrap >}}

{{< /flex >}}

{{< flex justify-content="space-between" centering=true >}}

{{< paypal >}}

<a href="https://www.gagives.org/organization/Sopo-Bicycle-Cooperative"><img src="/images/gagives.jpg" alt="GA Gives" title="Donate via Georgia Gives"/></a>

<div>
<img src="/images/yourcause.jpg" title="Donate via YourCause" alt="YourCause"/>
<p>Check with your employer and select Sopo Bicycle Cooperative as your charity of choice for YourCause account.</p>
</div>

{{< /flex >}}
